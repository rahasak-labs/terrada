package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"

	googleoauth "golang.org/x/oauth2/google"
	licensing "google.golang.org/api/licensing/v1"
	"google.golang.org/api/option"
	"google.golang.org/api/transport"
)

type Output struct {
	Assignments string `json:"assignments"`
}

func main() {
	ctx := context.Background()

	// retrieve customerId, impersonatedUserEmail, googleworkspaceSa  command-line arguments
	args := os.Args
	if len(args) < 4 {
		log.Fatalf("Missing required query parameters")
	}
	customerId := args[1]
	impersonatedUserEmail := args[2]
	sa := args[3]

	credParams := googleoauth.CredentialsParams{
		Scopes:  []string{"https://www.googleapis.com/auth/apps.licensing"},
		Subject: impersonatedUserEmail,
	}
	creds, err := googleoauth.CredentialsFromJSONWithParams(ctx, []byte(sa), credParams)
	if err != nil {
		log.Fatalf("fail google oauth: %v", err)
	}

	client, _, err := transport.NewHTTPClient(ctx, option.WithTokenSource(creds.TokenSource))
	if err != nil {
		log.Fatalf("fail transport client: %v", err)
	}

	service, err := licensing.NewService(ctx, option.WithHTTPClient(client))
	if err != nil {
		log.Fatalf("fail create licensing service: %v", err)
	}

	var assignments []licensing.LicenseAssignment
	var pageToken string
	productId := "Google-Apps"
	skuId := ""
	maxResults := 100
	for {
		resp, err := service.LicenseAssignments.ListForProductAndSku(productId, skuId, customerId).MaxResults(int64(maxResults)).PageToken(pageToken).Do()
		if err != nil {
			log.Fatalf("fail list license: %v", err)
		}

		for _, assignment := range resp.Items {
			assignments = append(assignments, *assignment)
		}

		if resp.NextPageToken == "" {
			break
		}

		pageToken = resp.NextPageToken
	}

	assignmentsJson, err := json.Marshal(assignments)
	if err != nil {
		log.Fatalf("Failed to marshal JSON: %v", err)
	}

	output := Output{
		Assignments: string(assignmentsJson),
	}

	outputJson, err := json.Marshal(output)
	if err != nil {
		log.Fatalf("Failed to marshal JSON: %v", err)
	}

	fmt.Println(string(outputJson))
}
