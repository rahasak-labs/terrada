data "external" "licenses" {
  program = ["./go/main", "3344223", "account-admin@rahasak.com", base64decode(var.googleworkspace_sa_base64)]
}

locals {
    all_licenses = jsondecode(data.external.licenses.result.assignments)
    enterprice_licenses = [for l in local.all_licenses : l if l.skuId == "1010010011"]
    starter_licenses = [for l in local.all_licenses : l if l.skuId == "1010010012"]
}

output "total_licenses_count" {
  value = length(local.all_licenses)
}

output "enterpice_licenses_count" {
  value = length(local.enterprice_licenses)
}

output "starter_licenses_count" {
  value = length(local.starter_licenses)
}

resource "null_resource" "license_notification" {
  triggers = {
    enterprice_license_count = (length(local.enterprice_licenses) <= 20)
  }

  provisioner "local-exec" {
    command = <<EOT
      # send email or notification command
      echo "Sending email/notification"
    EOT
  }
}
