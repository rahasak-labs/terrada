.PHONY: build
build:
	cd go && go build -o main *.go

.PHONY: init
init:
	terraform init

.PHONY: plan
plan:
	terraform plan -lock-timeout=10m -parallelism=30

.PHONY: apply
apply:
	terraform apply -auto-approve -no-color -parallelism=30
